﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MultipleViewMVVM.ViewModels.Commands
{
    public class RelayCommand: ICommand
    {
        public Action<object> ExecuteMethod { get; set; }
        public Func<object,bool> CanExecuteFunction { get; set; }

        public event EventHandler CanExecuteChanged;

        public RelayCommand( Action<object> executeMethod, Func<object, bool> canExecuteMethod)
        {
            this.ExecuteMethod = executeMethod;
            this.CanExecuteFunction = canExecuteMethod;
        }
        public bool CanExecute(object parameter)
        {
            return CanExecuteFunction(parameter);
        }

        public void Execute(object parameter)
        {
            ExecuteMethod(parameter);
        }
    }
}
