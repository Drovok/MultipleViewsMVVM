﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MultipleViewMVVM.ViewModels.Commands
{
    public class Change2BlueCommand : ICommand
    {
        public ViewModelBase ViewModel { get; set;}
        public event EventHandler CanExecuteChanged;

        public Change2BlueCommand(ViewModelBase viewModel)
        {
            this.ViewModel = viewModel;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //throw new NotImplementedException();
            //ViewModel.ChangeBackground2BlueMethod();
        }
    }
}
