﻿using MultipleViewMVVM.Models;
using MultipleViewMVVM.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleViewMVVM.ViewModels
{
    public class ViewModelBase: ObservableCollection<BocciaPlayer>, INotifyPropertyChanged
    {
        private ViewModelBase currentView;
        public ViewModelBase CurrentView
        {
            get
            {
                return currentView;
            }
            set
            {
                currentView = value;
                OnPropertyChanged("CurrentView");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Commands
        /// </summary>
        //public Change2BlueCommand Change2BlueCommand { get; set; }
        public RelayCommand change2blueCommand { get; set; }
        public RelayCommand change2redCommand { get; set; }
        public RelayCommand change2greenCommand { get; set; }
        public RelayCommand change2addViewCommand { get; set; }
        public RelayCommand removeFromListCommand { get; set; }

        public ViewModelBase()
        {

            for (int i = 1; i <= 2; i++)
            {
                Add(new BocciaPlayer()
                {
                    Name = "Person" + i,
                    Age = i,
                    Gender = Gender.Female,
                    Comments = "",
                    Photo_Path = ""
                });
            }

            Add(new BocciaPlayer()
            {
                Name = "Bruno Carvalho",
                Age = 46,
                Gender = Gender.Male,
                Comments = "Crazy !!!",
                Photo_Path = ""
            });

            //currentView = new BlueViewModel();
            //Change2BlueCommand = new Change2BlueCommand(this);
            change2blueCommand = new RelayCommand(ExecuteBlueCommand, CanExectuteBlueCommand);
            change2redCommand = new RelayCommand(ExecuteRedCommand, CanExectuteRedCommand);
            change2greenCommand = new RelayCommand(ExecuteGreenCommand, CanExectuteGreenCommand);
            change2addViewCommand = new RelayCommand(ExecuteAddCommand, CanExectuteAddCommand);
            removeFromListCommand = new RelayCommand(ExecuteRemoveCommand, CanExectuteRemoveCommand);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanExectuteBlueCommand(object param)
        {
            return true;
        }

        public void ExecuteBlueCommand(object param)
        {
            CurrentView = new BlueViewModel();
        }

        public bool CanExectuteRedCommand(object param)
        {
            return true;
        }

        public void ExecuteRedCommand(object param)
        {
            CurrentView = new RedViewModel();
        }

        public bool CanExectuteGreenCommand(object param)
        {
            return true;
        }

        public void ExecuteGreenCommand(object param)
        {
            CurrentView = new GreenViewModel();
        }

        public bool CanExectuteAddCommand(object param)
        {
            return true;
        }

        public void ExecuteAddCommand(object param)
        {
            CurrentView = new AddViewModel();
        }

        public bool CanExectuteRemoveCommand(object param)
        {
            return true;
        }

        public void ExecuteRemoveCommand(object param)
        {
            BocciaPlayer player = param as BocciaPlayer;
            Remove(player);

        }
        
        protected void AddToListView(BocciaPlayer player)
        {
            // Add(player);
           // var currentViewFromViewModelBase = ((ViewModelBase)System.Windows.Application.Current.MainWindow).CurrentView;

            //CurrentView = new RedViewModel();

            Add(new BocciaPlayer()
            {
                Name = "Bruno Carvalho",
                Age = 46,
                Gender = Gender.Male,
                Comments = "Crazy !!!",
                Photo_Path = ""
            });
            //OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }

    }
}
