﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleViewMVVM.Models
{
    public enum Gender { Male, Female };

    public class BocciaPlayer : INotifyPropertyChanged
    {
        private string name { get; set; }
        private int age { get; set; }
        private Gender gender { get; set; }
        private string comments { get; set; }
        private string photo_path { get; set; }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
                OnPropertyChanged("Age");
            }
        }

        public Gender Gender
        {
            get
            {
                return gender; //nameof(gender);
            }
            set
            {
                //gender = (Gender) Enum.Parse(typeof(Gender), value, true);
                gender = value;
                OnPropertyChanged("Gender");
            }
        }

        public string Comments
        {
            get
            {
                return comments;
            }
            set
            {
                comments = value;
                OnPropertyChanged("Commands");
            }
        }

        public string Photo_Path
        {
            get
            {
                return photo_path;
            }
            set
            {
                photo_path = value;
                OnPropertyChanged("Photo_Path");
            }
        }

        public BocciaPlayer()
        {

        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return Name;
        }
    };
}
